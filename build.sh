#!/usr/bin/env bash

set -eo pipefail

# The tags to use for the image.
declare -a TAGS=(dev)

# Add additional tags based on external env variable
if [[ "${IMAGE_PUBLISH:-}" == "yes" ]]; then
  if [[ "${IMAGE_RELEASE:-}" == "yes" ]]; then
    TAGS=("latest" "$(date +%Y%m%d%H%M)-$(git rev-parse --short HEAD)")

    git_tag=$(git describe --tags)
    if [[ "${git_tag}" != "" ]]; then
      TAGS+=("${git_tag}")
    fi
  else
    TAGS=("$(date +%Y%m%d%H%M)-$(git rev-parse --short HEAD)")
  fi
fi

IMAGE_REPO="${IMAGE_REPO:-artifacts.dox.support/infra-platform/db_url}"

docker run --privileged --rm tonistiigi/binfmt --install all
docker context create db_url || true
docker context use db_url
docker buildx create --use --driver docker-container db_url || true

declare -a build_args=()

for tag in "${TAGS[@]}"; do
  image_tag="${IMAGE_REPO}:${tag}"
  build_args+=("-t" "${image_tag}")
done

if [[ "${IMAGE_PUBLISH:-}" == "yes" ]]; then
  build_args+=("--push" "--platform=linux/arm64,linux/amd64")
else
  build_args+=("--load")
fi

docker buildx build "${build_args[@]}" --progress=plain .
