mod config;
mod driver;
mod error;

use crate::config::Config;
use crate::driver::{from_config, Driver};
use crate::error::Result;
use clap::Parser;

/// Parses a database connection URL, in the format of <driver>://[user[:pass]@]<host>[:port][/<database>],
/// invoking the appropriate CLI tool for the driver, and expand the rest of the information as parameters.
///
/// Examples:
///
///     # mysql --host=db.internal myapp_dev
///     db_url mysql://db.internal/myapp_dev
///
///     # mysql --host=db.internal --execute="SELECT * FROM mytable" myapp_dev
///     db_url mysql://db.internal/myapp_dev "SELECT * FROM mytable"
///     
///     # echo "SELECT * FROM mytable" | mysql --host=db.internal myapp_dev
///     echo "SELECT * FROM mytable" | db_url mysql://db.internal/myapp_dev
///
///     # mysql --user=user --password=pass --host=db.internal myapp_dev
///     db_url mysql://user:pass@db.internal/myapp_dev
///
///     # mysql --user=user --password=pass --host=db.internal myapp_test
///     db_url mysql://user:pass@db.internal/myapp_dev -d myapp_test
#[derive(Parser, Debug)]
#[clap(verbatim_doc_comment)]
#[command(version, about)]
struct App {
    /// Database URL to use. i.e. mysql://user:pass@host/dbname
    url: String,

    /// Executes this query instead of opening the prompt
    query: Option<String>,

    /// Instead of connecting, output the parsed config in JSON
    #[arg(long)]
    output_json: bool,

    /// Override the database
    #[arg(short, long)]
    database: Option<String>,
}

fn main() {
    let app = App::parse();

    dispatch(app).unwrap_or_else(|err| {
        eprintln!("Error: {}", err);
        std::process::exit(1);
    });
}

fn dispatch(app: App) -> Result<()> {
    let mut config = Config::from_url(&app.url)?;
    config.query = app.query;

    if app.database.is_some() {
        config.database = app.database;
    }

    if app.output_json {
        println!("{}", serde_json::to_string(&config)?);
        return Ok(());
    }

    let driver = from_config(&config)?;
    driver.run()
}
