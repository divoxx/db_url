use crate::error::Result;
use serde::Serialize;
use urlencoding::decode;

/// Holds the entire configuration for the program, based on the CLI arguments passed in.
#[derive(Debug, PartialEq, Eq, Serialize)]
pub(crate) struct Config {
    pub scheme: String,
    pub host: Option<String>,
    pub port: Option<u16>,
    pub username: Option<String>,
    pub password: Option<String>,
    pub database: Option<String>,
    pub query: Option<String>,
}

impl Config {
    pub fn from_url(url_str: &str) -> Result<Config> {
        let url = url::Url::parse(url_str)?;

        Ok(Config {
            scheme: url.scheme().to_owned(),
            host: url.host_str().map(|host| host.to_string()),
            port: url.port(),
            username: decode(url.username()).ok().map(|s| s.into_owned()),
            password: url
                .password()
                .and_then(|s| decode(s).ok().map(|s| s.into_owned())),
            database: url
                .path_segments()
                .and_then(|mut iter| iter.nth(0).map(|seg| seg.to_string())),
            query: None,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_parses_url_into_config() {
        let url = "mysql://user:pass@host/name";

        assert_eq!(
            Config::from_url(url).unwrap(),
            Config {
                scheme: "mysql".to_owned(),
                host: Some("host".to_owned()),
                port: None,
                username: Some("user".to_owned()),
                password: Some("pass".to_owned()),
                database: Some("name".to_owned()),
                query: None,
            }
        )
    }

    #[test]
    fn it_parses_url_with_improperly_url_encoded_password() {
        let url = "mysql://user:p:ass@host/name";
        let cfg = Config::from_url(url).unwrap();

        assert_eq!(cfg.password, Some("p:ass".to_owned()))
    }

    #[test]
    fn it_parses_url_with_properly_url_encoded_password() {
        let url = "mysql://user:p%3Aass@host/name";
        let cfg = Config::from_url(url).unwrap();

        assert_eq!(cfg.password, Some("p:ass".to_owned()))
    }

    #[test]
    fn it_parses_url_with_improperly_url_encoded_username() {
        let url = "mysql://u@ser:pass@host/name";
        let cfg = Config::from_url(url).unwrap();

        assert_eq!(cfg.username, Some("u@ser".to_owned()))
    }

    #[test]
    fn it_parses_url_with_properly_url_encoded_username() {
        let url = "mysql://u%3Aser:pass@host/name";
        let cfg = Config::from_url(url).unwrap();

        assert_eq!(cfg.username, Some("u:ser".to_owned()))
    }
}
