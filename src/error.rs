use thiserror::Error;

pub(crate) type Result<T> = eyre::Result<T>;

#[derive(Debug, Error)]
pub(crate) enum AppError {
    #[error("unsupported driver {driver_name}")]
    UnsupportedDriver { driver_name: String },
}
