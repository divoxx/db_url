use crate::config::Config;
use crate::error::{AppError, Result};
use eyre::eyre;
use std::process::Command;

pub(crate) trait Driver {
    fn run(&self) -> Result<()>;
}

pub(crate) fn from_config<'a>(config: &'a Config) -> Result<impl Driver + 'a> {
    match config.scheme.as_ref() {
        "mysql" | "mysql2" => Ok(MySQLDriver { config }),

        other => Err(AppError::UnsupportedDriver {
            driver_name: other.to_string(),
        })?,
    }
}

struct MySQLDriver<'a> {
    config: &'a Config,
}

impl<'a> Driver for MySQLDriver<'a> {
    fn run(&self) -> Result<()> {
        let mut cmd = Command::new("mysql");

        if self.config.host.is_some() {
            cmd.arg(format!("--host={}", self.config.host.as_ref().unwrap()));
        }

        if self.config.port.is_some() {
            cmd.arg(format!("--port={}", self.config.port.as_ref().unwrap()));
        }

        if self.config.username.is_some() {
            cmd.arg(format!("--user={}", self.config.username.as_ref().unwrap()));
        }

        if self.config.password.is_some() {
            cmd.arg(format!(
                "--password={}",
                self.config.password.as_ref().unwrap()
            ));
        }

        if self.config.query.is_some() {
            cmd.arg(format!("--execute={}", self.config.query.as_ref().unwrap()));
        }

        if self.config.database.is_some() {
            cmd.arg(format!("{}", self.config.database.as_ref().unwrap()));
        }

        let status = cmd.status()?;

        if status.success() {
            Ok(())
        } else {
            Err(eyre!("mysql errored"))
        }
    }
}
