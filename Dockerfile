FROM rust:1-alpine as builder
WORKDIR /app 
RUN apk add --no-cache bash musl-dev
COPY . .

ARG TARGETARCH

SHELL ["/bin/bash", "-c"] 
RUN arch="${TARGETARCH}"; \
  arch=${arch/amd64/x86_64}; \
  arch=${arch/arm64/aarch64}; \
  env RUSTFLAGS='-C target-feature=+crt-static' cargo build --release --target="${arch}-unknown-linux-musl"

FROM debian:bullseye
COPY --from=builder /app/target/*-linux-musl/release/db_url /usr/local/bin/db_url
ENTRYPOINT ["db_url"]
